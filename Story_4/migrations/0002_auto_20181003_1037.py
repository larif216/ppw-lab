# Generated by Django 2.1.1 on 2018-10-03 03:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Story_4', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guest',
            name='date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
