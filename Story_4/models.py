from django.db import models

# Create your models here.
class Guest(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField()
    subject = models.CharField(max_length=27)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

