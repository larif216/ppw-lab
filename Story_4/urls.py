from django.urls import path
from .views import index, about, resume, diary, contact, form, add_guest

#url for app, add your URL Configuration

app_name = 'Story_4'
urlpatterns = [
    path('', index, name='index'),
    path('About', about, name='about'),
    path('Resume', resume, name='resume'),
    path('Contact', contact, name='contact'),
    path('Form', form, name='form'),
    path('add_guest', add_guest, name='add_guest'),
]