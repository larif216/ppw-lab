from django.shortcuts import render, redirect
from .models import Guest

# Create your views here.
def index (request):
    response = {}
    return render(request, 'home.html', response)

def about (request):
    return render(request, 'about.html')

def resume (request):
    return render(request, 'resume.html')

def contact (request):
    return render(request, 'contact.html')

def diary (request):
    return redirect('Story_5:diary')

def form (request):
    return render(request, 'form.html')

def add_guest (request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        subject = request.POST['subject']
        message = request.POST['message']
        guest = Guest(name=name, email=email, subject=subject, message=message)
        guest.save()
    return redirect('Story_4:form')