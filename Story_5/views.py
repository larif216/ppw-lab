from django.shortcuts import render, redirect
from .forms import DiaryForm
from .models import Diary


def CreateDiary(request):
    response = {}
    schedule = DiaryForm(request.POST or None)
    if (request.method == 'POST' and schedule.is_valid()):
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        schedule_form = Diary(name=response['name'],
                              date=response['date'],
                              location=response['location'],
                              category=response['category'])
        schedule_form.save()
        return redirect('Story_5:diary')
    else:
        return redirect('Story_5:diary-form')

def DiaryFormViews(request):
    response = {'form': DiaryForm}
    return render(request, "diaryForm.html", response)

def MyDiary(request):
    response = {}
    data = Diary.objects.all()
    response['data'] = data
    return render(request, 'diary.html', response)

def DeleteDiary(request):
    response = {}
    reset = Diary.objects.all().delete()
    response['reset'] = reset
    return redirect('Story_5:diary')