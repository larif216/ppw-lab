from django import forms

class DiaryForm(forms.Form):
    name = forms.CharField(label='Name', max_length=27, required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    location = forms.CharField(label='Location', max_length=27, required=True,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
    category = forms.CharField(label='Category', max_length=27, required=True,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
    date = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}),
                               input_formats=['%Y-%m-%dT%H:%M'])
