from django.db import models


class Diary(models.Model):
    name = models.CharField(max_length=27)
    date = models.DateTimeField()
    location = models.CharField(max_length=27)
    category = models.CharField(max_length=27)

    class Meta:
        verbose_name_plural = 'Diary'

    def __str__(self):
        return self.name
