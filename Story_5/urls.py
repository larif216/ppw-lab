from django.urls import path
from .views import CreateDiary, MyDiary, DiaryFormViews, DeleteDiary

#url for app, add your URL Configuration

app_name = 'Story_5'
urlpatterns = [
    path('', MyDiary, name='diary'),
    path('diary-form', DiaryFormViews, name='diary-form'),
    path('create-diary', CreateDiary, name='add_activity'),
    path('my-diary', MyDiary, name='diary'),
    path('delete-diary', DeleteDiary, name='delete_diary'),
]