from django.shortcuts import render
from datetime import datetime, date

# Create your views here.
def index(request):
    response ={
        'my_bio':my_bio,
        'friend_bio':friend_bio,
        'my_name':my_name,
        'friend_name':friend_name,
    }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

my_name = 'Muhamad Lutfi Arif'
friend_name = 'Muhammad Andriansyah Putra'
curr_year = int(datetime.now().strftime("%Y"))
my_birth_date = date(1999, 10, 23)
friend_birth_date = date(1999, 8, 22)
my_npm = '1706979360'
friend_npm = '1706044093'

my_bio = [
    {'key':'Age', 'value':calculate_age(my_birth_date.year)},
    {'key':'NPM', 'value':my_npm},
    {'key':'College', 'value':'University of Indonesia'},
    {'key':'Hobbies', 'value':'Read books, Watch movies, Gaming, Coding'},
    {'key':'Description', 'value':"I am learning to be an expert web developer right now."},
]
friend_bio = [
    {'key':'Age', 'value':calculate_age(friend_birth_date.year)},
    {'key':'NPM', 'value':friend_npm},
    {'key':'College', 'value':'University of Indonesia'},
    {'key':'Hobbies', 'value':'Watch Football, Music, Play Football'},
    {'key':'Description', 'value':"Quran FUKI staff"},
]