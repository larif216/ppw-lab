from django.urls import path
from .views import index

#url for app
app_name = 'Story_1'
urlpatterns = [
    path('', index, name='index'),
]